#!/bin/sh

set -e

CE_TRIVY_DB_REGISTRY="ghcr.io/aquasecurity/trivy-db"
EE_TRIVY_DB_REGISTRY="registry.gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad"

setup_trivy_files() {
  echo "Creating temp directory"
  trivy_version=$(cat TRIVY_VERSION)
  trivy_db_version_ce=$(cat TRIVY_DB_VERSION_CE)
  trivy_db_version_ee=$(cat TRIVY_DB_VERSION_EE)
  echo "Dowloading and installing Trivy ${trivy_version}"
  mkdir /home/gitlab/opt/trivy
  wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v"${trivy_version}"/trivy_"${trivy_version}"_Linux-64bit.tar.gz -O - | tar -zxvf - -C /home/gitlab/opt/trivy
  ln -s /home/gitlab/opt/trivy/trivy /home/gitlab/trivy

  echo "Setting up Trivy files"
  mkdir -p /home/gitlab/.cache/trivy/ce/db /home/gitlab/.cache/trivy/ee/db
  mv /home/gitlab/legal /home/gitlab/.cache/trivy

  echo "Dowloading CE Trivy DB"
  mkdir -p /tmp/trivy-ce
  oras pull "$CE_TRIVY_DB_REGISTRY":"${trivy_db_version_ce}" -a -o /tmp/trivy-ce

  echo "Dowloading EE Trivy DB"
  mkdir -p /tmp/trivy-ee
  oras pull "$EE_TRIVY_DB_REGISTRY":"${trivy_db_version_ee}" -a -o /tmp/trivy-ee

  chmod -R g+rw /home/gitlab/.cache/
}

select_scanner() {
  # The following conditionals will have be update to accomodate a new scanner.
  # Note that files under download folder are coming from the previous docker stage.
  # The default should always point to trivy
  setup_trivy_files
}

mkdir /home/gitlab/opt
select_scanner
